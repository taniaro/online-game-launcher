﻿namespace Contracts
{
    public class UserGameDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
