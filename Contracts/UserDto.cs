﻿namespace Contracts
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public int Permission { get; set; }
        public string Picture { get; set; }
    }
}
