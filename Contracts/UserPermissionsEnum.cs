﻿namespace Contracts
{
    enum UserPermissions
    {
        Admin = 1,
        User = 2
    }
}
