﻿namespace Contracts
{
    public class GameDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }

        public byte[] Picture { get; set; }
        public byte[] Screenshot { get; set; }
    }
}
