# Online game launcher

Install on your computer:

1. Download git for your computer: 
	* https://git-scm.com/downloads
1. Install NodeJS
	* https://nodejs.org/uk/
1. Install MySQL - you need Server, Workbench, for VisualStudio and Connector/.NET
	* https://dev.mysql.com/downloads/installer/
1. Create folder where you want to have a project. Open terminal 
		/ right-click your mouse and open Git Bash here.
1. Clone repository:
	* git clone https://gitlab.com/taniaro/online-game-launcher.git
		* If you have error "fatal: I don't handle protocol 'https'" - remove 
			space after clone and re-enter it. Should work.

1. Go to MySQL Workbench and import database (ask me for copy):
	1. Create empty database: New schema icon -> Name - ogldb, Default charset
		- latin1, Default collation - latin1_bin -> Apply
	1. Import existing data to new DB: Server -> Data import -> Choose folder with DB -> Start import

1. Changes in project: 
	1. Find the file appsettings.json and open it
	1. Find row "ConnectionStrings": {
    "DefaultConnection": "server=localhost;user id=root;password=mypassword;port=3306;database=ogldb;"
  },
	1. Change user id and password to your MySQL credentials
	1. Go to file .gitignore
	1. Write appsettings.json down there if it isn't here yet

1. Now you can open project, create branches and have fun!
