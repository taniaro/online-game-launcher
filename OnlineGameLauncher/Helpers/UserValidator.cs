﻿using Contracts;
using FluentValidation;
                        
namespace OnlineGameLauncher.Helpers
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .Length(3, 30)
                .WithMessage("Name should contain 3-30 symbols")
                .Must(NoCyrillic)
                .WithMessage("Cyrillic symbols are not allowed");

            RuleFor(x => x.Password)
                .Length(6, 30)
                .WithMessage("Pasword must contain 6-30 symbols")
                .NotNull()
                .Must(NoCyrillic)
                .WithMessage("Cyrillic symbols are not allowed");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email address is required")
                .EmailAddress()
                .WithMessage("A valid email is required")
                .Must(NoCyrillic)
                .WithMessage("Cyrillic symbols are not allowed");
        }

        private bool NoCyrillic(string text)
        {
            return !StringHelper.IncludesCyrillicSymbols(text);
        }
    }
}
