﻿using System.Text.RegularExpressions;

namespace OnlineGameLauncher.Helpers
{
    public class StringHelper
    {
        public static bool IncludesCyrillicSymbols(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (Regex.IsMatch(text, @"\p{IsCyrillic}"))
                {
                    return true;
                }
            }
            return false;
        }
    }
}