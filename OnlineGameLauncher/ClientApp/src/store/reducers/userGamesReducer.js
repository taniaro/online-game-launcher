import {
    ADD_USER_GAME, 
    GET_USER_GAMES,
    DELETE_USER_GAME,
    USER_GAME_ERROR,
    storedState
  } from '../actions/types';
  
  const initialState =
    storedState !== null
      ? storedState.userGames
      : {
          userGames: null,
          userGameError: null
        };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case GET_USER_GAMES:
        return {
          ...state,
          userGames: action.payload
        };
      case ADD_USER_GAME:
        return {
          ...state,
          userGames: action.payload
        };
        case DELETE_USER_GAME:
            console.log(state)
            console.log(action)
        return {
          ...state,
          userGames: state.userGames.filter(game => game.id !== action.payload),
        };
      case USER_GAME_ERROR:
        return {
          ...state,
          userGameError: action.payload
        };
      default:
        return state;
    }
  };