import { combineReducers } from 'redux';
import authReducer from './authReducer';
import gamesReducer from './gamesReducer'
import userGamesReducer from './userGamesReducer'

export default combineReducers({
  // List of reducers
  auth: authReducer,
  games: gamesReducer,
  userGames: userGamesReducer
});
