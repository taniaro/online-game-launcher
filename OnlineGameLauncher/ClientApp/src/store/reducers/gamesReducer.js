import {
  ADD_GAME,
  GET_GAMES,
  GET_SINGLE_GAME,
  UPDATE_GAME,
  DELETE_GAME,
  GAME_ERROR,
  storedState
} from '../actions/types';

const initialState =
  storedState !== null
    ? storedState.games
    : {
        games: null,
        currentGame: null,
        gameError: null
      };

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_GAMES:
      return {
        ...state,
        currentGame: null,
        games: action.payload
      };
    case GET_SINGLE_GAME:
      return {
        ...state,
        currentGame: action.payload
      };
    case ADD_GAME:
      return {
        ...state,
        games: [action.payload, ...state.games]
      };
    case DELETE_GAME:
      return {
        ...state,
        currentGame: null,
        games: state.games.filter(game => game.id !== action.payload),
      };
    case UPDATE_GAME:
      return {
        ...state,
        currentGame: null
      };
    case GAME_ERROR:
      return {
        ...state,
        gameError: action.payload
      };
    default:
      return state;
  }
};