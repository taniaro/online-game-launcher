import {
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS,
  storedState,
} from '../actions/types';
const initialState =
  storedState !== null
    ? storedState.auth
    : {
        token: localStorage.getItem('token'),
        loading: false,
        user: null,
        isAuthenticated: false,
        error: null,
      };

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUEST:
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        token: action.payload.token,
        loading: false,
        isAuthenticated: true,
        user: {
          name: action.payload.name,
          email: action.payload.email,
          picture: action.payload.picture,
          id: action.payload.id,

        },
      };
    case REGISTER_FAIL:
    case LOGIN_FAIL:
      localStorage.removeItem('token');
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
        user: null,
        error: action.payload,
      };
    case LOGOUT:
      localStorage.removeItem('token');
      return {
        ...state,
        token: localStorage.getItem('token'),
        loading: false,
        user: null,
        isAuthenticated: false,
        error: null,
      };
    case CLEAR_ERRORS: {
      return {
        ...state,
        error: null,
      };
    }
    default:
      return state;
  }
};
