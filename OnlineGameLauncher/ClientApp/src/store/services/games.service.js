import axios from 'axios';

export const getGamesReq = async () => {
  const res = await axios.get('/api/Games');
  return res.data;
};
export const getSingleGameReq = async id => {
  const res = await axios.get(`/api/Games/${id}`, {
    params: {
      id: id,
    },
  });
  return res.data;
};
export const addGameReq = async formData => {
  const fd = new FormData();
  fd.append('name', formData.name);
  fd.append('link', formData.link);
  fd.append('description', formData.description);
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = await axios.post('/api/Games/creategame', fd, config);
  return res.data;
};
export const deleteGameReq = async id => {
  await axios.delete(`/api/Games/${id}`, {
    params: {
        id: id,
      },
  });
};
export const updateGameReq = async formData => {
  console.log(formData);
  const fd = new FormData();
  fd.append('name', formData.name);
  fd.append('link', formData.link);
  fd.append('description', formData.description);

  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = await axios.put(`/api/Games/${formData._id}`, fd, config);
  return res.data;
};