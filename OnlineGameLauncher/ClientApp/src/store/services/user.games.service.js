import axios from 'axios';

export const getUserGamesReq = async userId => {
  const res = await axios.get(`/api/UserGames/${userId}`,{params:{
    userId: userId
  }});
  return res.data;
};
export const addUserGameReq = async formData => {
  console.log(formData);
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = await axios.post('/api/UserGames', formData, config);
  return res.data;
};
export const deleteUserGameReq = async id => {
  console.log(id);
  const res = await axios.delete(`/api/UserGames/${id}`,{ params :{
    id: id
  }});
  console.log(res.data);
  return res.data;
};
