import axios from 'axios';

export const registerAuth = async formData => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = await axios.post('/api/Users/register', formData, config);
  return res.data;
};

export const loginAuth = async formData => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = await axios.post('/api/Users/authenticate', formData, config);
  return res.data;
};
