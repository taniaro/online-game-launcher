import { call, put, all, takeEvery } from 'redux-saga/effects';
import {
  getGamesReq,
  getSingleGameReq,
  addGameReq,
  deleteGameReq,
  updateGameReq,
} from '../services/games.service';
import {
  getGamesSuccess,
  getSingleGameSuccess,
  addGameSuccess,
  deleteGameSuccess,
  updateGameSuccess,
  GameError
} from '../actions/game.actions';
import {history} from '../../components/App'

function* getGames() {
  try {
    const data = yield call(getGamesReq);
    yield put(getGamesSuccess(data));
  } catch (error) {
    yield put(GameError(error));
    console.log(error);
  }
}
function* getSingleGame(request) {
  try {
    const data = yield call(getSingleGameReq, request.payload);
    yield put(getSingleGameSuccess(data));
  } catch (error) {
    yield put(GameError(error));
    console.log(error);
  }
}
function* addGame(request) {
  try {
    console.log(request.payload);
    const data = yield call(addGameReq, request.payload);
    console.log(data);
    yield put(addGameSuccess(data));
  } catch (error) {
    yield put(GameError(error));
    console.log(error);
  }
}
function* deleteGame(request) {
  try {
    console.log(request);
    yield call(deleteGameReq, request.payload);
    yield put(deleteGameSuccess(request.payload.id));
  } catch (error) {
    yield put(GameError(error));
    console.log(error);
  }
}
function* updateGame(request) {
  try {
    console.log(request);
    const data = yield call(updateGameReq, request.payload);
    console.log(data);
    yield put(updateGameSuccess(data));
  } catch (error) {
    yield put(GameError(error));
    console.log(error);
  }
}
export function* gamesSaga() {
  yield all([
    takeEvery('GET_GAMES_REQUEST', getGames),
    takeEvery('GET_SINGLE_GAME_REQUEST', getSingleGame),
    takeEvery('ADD_GAME_REQUEST', addGame),
    takeEvery('DELETE_GAME_REQUEST', deleteGame),
    takeEvery('UPDATE_GAME_REQUEST', updateGame),
  ]);
}