import { all, put, call, takeEvery } from 'redux-saga/effects';
import {
  registerSuccess,
  registerFail,
  loginSuccess,
  loginFail,
  logout,
} from '../actions/auth.actions';

import { registerAuth, loginAuth } from '../services/auth.service';
import {history} from '../../components/App'

function* register(request) {
  try {
    const data = yield call(registerAuth, request.payload);
    yield put(registerSuccess(data));
    history.push('/login')
  } catch (error) {
    yield put(registerFail(error));
    console.log(error);
  }
}

function* login(request) {
  try {
    const data = yield call(loginAuth, request.payload);
    yield put(loginSuccess(data));
  } catch (error) {
    yield put(loginFail(error));
    yield put(logout());
    console.log(error);
  }
}

export function* authSaga() {
  yield all([takeEvery('REGISTER_REQUEST', register), takeEvery('LOGIN_REQUEST', login)]);
}
