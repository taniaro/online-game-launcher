import { call, put, all, takeEvery } from 'redux-saga/effects';
import {
  getUserGamesReq,
  addUserGameReq,
  deleteUserGameReq,
} from '../services/user.games.service';
import {
  getUserGamesSuccess,
  addUserGameSuccess,
  deleteUserGameSuccess,
  UserGameError
} from '../actions/user.game.actions';
import {history} from '../../components/App'

function* getUserGames(request) {
  try {
    console.log(request.payload);
    const data = yield call(getUserGamesReq, request.payload);
    console.log(data)
    yield put(getUserGamesSuccess(data));
  } catch (error) {
    yield put(UserGameError(error));
    console.log(error);
  }
}
function* addUserGame(request) {
  try {
    console.log(request.payload);
    const data = yield call(addUserGameReq, request.payload);
    console.log(data);
    yield put(addUserGameSuccess(data));
  } catch (error) {
    yield put(UserGameError(error));
    console.log(error);
  }
}
function* deleteUserGame(request) {
  try {
    console.log(request);
      yield call(deleteUserGameReq, request.payload)
    yield put(deleteUserGameSuccess(request.payload));
  } catch (error) {
    yield put(UserGameError(error));
    console.log(error);
  }
}
export function* userGamesSaga() {
  yield all([
    takeEvery('GET_USER_GAMES_REQUEST', getUserGames),
    takeEvery('ADD_USER_GAME_REQUEST', addUserGame),
    takeEvery('DELETE_USER_GAME_REQUEST', deleteUserGame)
  ]);
}