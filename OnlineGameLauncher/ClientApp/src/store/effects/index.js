import { all, fork } from 'redux-saga/effects';
import { authSaga } from './auth.effects';
import { gamesSaga } from './games.effects';
import {userGamesSaga} from './user.games.effects'
import { statePersistSaga } from './store_persist.effect';
export default function* rootSaga() {
  yield all([
    // list of effects
    fork(authSaga),
    fork(gamesSaga),
    fork(userGamesSaga),
    fork(statePersistSaga),
  ]);
}
