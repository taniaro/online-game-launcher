/* SAGA FOR SAVING STATE IN LOCAL STORAGE */
import { select, takeEvery, call, all } from 'redux-saga/effects';

const identify = action => {
  return action;
};

const save = state => window.localStorage.setItem('persisted-state', JSON.stringify(state));

function* persistState() {
  const state = yield select(identify);
  yield call(save, state);
}

export function* statePersistSaga() {
  yield all([
    takeEvery(
      [
        'REGISTER_REQUEST',
        'REGISTER_SUCCESS',
        'REGISTER_FAIL',
        'LOGIN_REQUEST',
        'LOGIN_SUCCESS',
        'LOGIN_FAIL',
        'LOGOUT',
        'CLEAR_ERRORS',

        'ADD_GAME_REQUEST',
        'ADD_GAME',
        'GET_GAMES_REQUEST',
        'GET_GAMES',
        'GET_SINGLE_GAME_REQUEST',
        'GET_SINGLE_GAME',
        'DELETE_GAME_REQUEST',
        'DELETE_GAME',
        'UPDATE_GAME_REQUEST',
        'UPDATE_GAME',
        'GAME_ERROR',

        'ADD_USER_GAME_REQUEST', 
        'ADD_USER_GAME', 
        'GET_USER_GAMES_REQUEST', 
        'GET_USER_GAMES', 
        'DELETE_USER_GAME_REQUEST',
        'DELETE_USER_GAME',
        'USER_GAME_ERROR'
      ],
      persistState,
    ),
  ]);
}
