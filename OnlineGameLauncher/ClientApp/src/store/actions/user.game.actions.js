import {
    ADD_USER_GAME_REQUEST, 
    ADD_USER_GAME, 
    GET_USER_GAMES_REQUEST, 
    GET_USER_GAMES, 
    DELETE_USER_GAME_REQUEST,
    DELETE_USER_GAME,
    USER_GAME_ERROR,
} from './types'

export const getUserGamesRequest = id => {
    return {
      type: GET_USER_GAMES_REQUEST,
      payload: id
    };
  };
  export const getUserGamesSuccess = data => {
    return {
      type: GET_USER_GAMES,
      payload: data,
    };
  };
  export const addUserGameRequest = formData => {
    return {
      type: ADD_USER_GAME_REQUEST,
      payload: formData
    };
  };
  export const addUserGameSuccess = formData => {
    return {
      type: ADD_USER_GAME,
      payload: formData,
    };
  };
export const deleteUserGameRequest = id => {
      console.log('DUGR')
    return {
      type: DELETE_USER_GAME_REQUEST,
      payload: id,
    };
  };
export const deleteUserGameSuccess = id => {
    console.log(id)
    console.log('^ DUGS')
    return {
      type: DELETE_USER_GAME,
      payload: id,
    };
  };
  export const UserGameError = error => {
    return {
      type:  USER_GAME_ERROR,
      payload: error.response.data.msg,
    };
  };