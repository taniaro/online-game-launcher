import {
    ADD_GAME_REQUEST,
    ADD_GAME,
    GET_GAMES_REQUEST,
    GET_GAMES,
    GET_SINGLE_GAME_REQUEST,
    GET_SINGLE_GAME,
    DELETE_GAME_REQUEST,
    DELETE_GAME,
    UPDATE_GAME_REQUEST,
    UPDATE_GAME,
    GAME_ERROR
} from './types'

export const getGamesRequest = () => {
    return {
      type: GET_GAMES_REQUEST,
    };
  };
  export const getGamesSuccess = data => {
    return {
      type: GET_GAMES,
      payload: data,
    };
  };
  export const getSingleGameRequest = id => {
    return {
      type: GET_SINGLE_GAME_REQUEST,
      payload: id,
    };
  };
  export const getSingleGameSuccess = data => {
    return {
      type: GET_SINGLE_GAME,
      payload: data,
    };
  };
  export const addGameRequest = formData => {
    return {
      type: ADD_GAME_REQUEST,
      payload: formData,
    };
  };
  export const addGameSuccess = formData => {
    return {
      type: ADD_GAME,
      payload: formData,
    };
  };
  export const deleteGameRequest = data => {
    return {
      type: DELETE_GAME_REQUEST,
      payload: data,
    };
  };
  export const deleteGameSuccess = id => {
    return {
      type: DELETE_GAME,
      payload: id,
    };
  };
  export const updateGameRequest = data => {
    return {
      type: UPDATE_GAME_REQUEST,
      payload: data,
    };
  };
  export const updateGameSuccess = data => {
    return {
      type: UPDATE_GAME,
      payload: data,
    };
  };
  export const GameError = error => {
    return {
      type: GAME_ERROR,
      payload: error.response.data.msg,
    };
  };