export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const storedState =
  window.localStorage.getItem('persisted-state') !== 'undefined'
    ? JSON.parse(window.localStorage.getItem('persisted-state'))
    : null;
export const ADD_GAME_REQUEST = 'ADD_GAME_REQUEST';
export const ADD_GAME = 'ADD_GAME';
export const GET_GAMES_REQUEST = 'GET_GAMES_REQUEST';
export const GET_GAMES = 'GET_GAMES';
export const GET_SINGLE_GAME_REQUEST = 'GET_SINGLE_GAME_REQUEST';
export const GET_SINGLE_GAME = 'GET_SINGLE_GAME';
export const DELETE_GAME_REQUEST = 'DELETE_GAME_REQUEST';
export const DELETE_GAME = 'DELETE_GAME';
export const UPDATE_GAME_REQUEST = 'UPDATE_GAME_REQUEST';
export const UPDATE_GAME = 'UPDATE_GAME';
export const GAME_ERROR = 'GAME_ERROR';

export const ADD_USER_GAME_REQUEST = 'ADD_USER_GAME_REQUEST';
export const ADD_USER_GAME = 'ADD_USER_GAME';
export const GET_USER_GAMES_REQUEST = 'GET_USER_GAMES_REQUEST';
export const GET_USER_GAMES = 'GET_USER_GAMES';
export const DELETE_USER_GAME_REQUEST = 'DELETE_USER_GAME_REQUEST';
export const DELETE_USER_GAME = 'DELETE_USER_GAME';
export const USER_GAME_ERROR = 'USER_GAME_ERROR';