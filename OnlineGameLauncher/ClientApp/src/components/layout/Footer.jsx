import React from 'react';
import { Link } from 'react-router-dom';
import { FooterComponent } from '../StyledComponents';
export const Footer = () => {
  return (
    <FooterComponent >
      <Link to='/policy'>
        Copyright &copy; 2020. Privacy Policy
      </Link>
    </FooterComponent>
  );
};
export default Footer;
