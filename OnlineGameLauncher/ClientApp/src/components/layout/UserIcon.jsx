import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Icon from '../../res/icon.svg';

const styling = {
    borderRadius: '50%',
}
const UserIcon = props => {
    const {
        auth: { user },
      } = props;
    return (
        <Fragment>
            <Link to='/profile'>
                <img src = {`data:image/jpeg;base64,${user.picture}`} alt="icon" style={styling} width="50" height="50"/>
            </Link>
        </Fragment>
    )
}
const mapStateToProps = state => ({
    auth: state.auth,
  });
export default connect(mapStateToProps)(UserIcon);