import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../store/actions/auth.actions';
import Logo from '../../res/game-controller.svg';
import UserIcon from './UserIcon';
import { NavbarWrapper, Button } from '../StyledComponents';
export const Header = props => {
  const {
    auth: { isAuthenticated },
    logout,
  } = props;
  const onLogout = () => {
    logout();
  };
  return (
    <Fragment>
      <header>
        <nav style={{ backgroundColor: '#3e3961' }}>
            <NavbarWrapper>
                      {isAuthenticated && <UserIcon />}
            <Link to='/'>
              <img src={Logo} width='150' height='75' alt='logo' style={{ cursor: 'pointer' }} />
            </Link>
            <ul className='nav-container'>
              {isAuthenticated ? (
                <Fragment>

                  <li>
                    <Button onClick={onLogout}>Logout</Button>
                  </li>
                </Fragment>
              ) : null}
            </ul>
          </NavbarWrapper>
        </nav>
      </header>
    </Fragment>
  );
};
const mapStateToProps = state => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { logout })(Header);
