import React,{Fragment,useEffect} from 'react'
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import { ContentWrapper,GamesWrapper} from '../StyledComponents'
import UserGameItem from '../games/UserGameItem';
import { connect } from 'react-redux';
import { getUserGamesRequest,deleteUserGameSuccess } from '../../store/actions/user.game.actions';

const UserProfile = props => {
    const {auth:{ user },userGames:{userGames},getUserGamesRequest } = props;
    const { name, email, picture, id } = user;

    return (
        <Fragment>
        <Header/>
        <ContentWrapper>
        {user !== null ? (
                    <Fragment>
                        <div style={{ marginTop: '20px' }} > <img src = {`data:image/jpeg;base64,${picture}`} alt="icon" width="200" height="200"/> </div>
                        
            <ul style={{marginTop:'18px'}}>
              <li> {name}</li>
              <li> {email}</li>
            </ul>
          <GamesWrapper>
          {userGames !== null ? (
            <Fragment>
              {userGames.map(userGame => (
                <UserGameItem userGame={userGame} key={userGame.id} />
              ))}
            </Fragment>
          ) : null}
        </GamesWrapper>
        </Fragment>
        ) : null}
        </ContentWrapper>
        <Footer />
      </Fragment>
    )
}
const mapStateToProps = state => ({
    auth: state.auth,
    userGames: state.userGames
  });
export default connect(mapStateToProps,{getUserGamesRequest})(UserProfile);