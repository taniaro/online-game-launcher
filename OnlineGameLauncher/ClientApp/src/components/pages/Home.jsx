import React, { Fragment } from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import { ContentWrapper} from '../StyledComponents'
import Games from '../games/Games'

const Home = () => {
  return (
    <Fragment>
      <Header/>
      <ContentWrapper>
        <Games/>
      </ContentWrapper>
      <Footer />
    </Fragment>
  );
};

export default Home;
