import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import showPassIcon from '../../res/show-pass-icon.svg';
import showHidePassword from '../../utils/Show-HidePassword';
import { loginRequest, clearErrors } from '../../store/actions/auth.actions';
import { ContentWrapper, Card,CardLabel,CardForm,FormInput,FormSubmit } from '../StyledComponents';


const redirectLabel = {
  color: '#349a89',
  textTransform: 'uppercase',
  cursor: 'pointer'
}


const Login = props => {
  const {
    auth: { isAuthenticated, error },
    loginRequest,
    clearErrors,
  } = props;
  useEffect(() => {
    if (isAuthenticated) {
      props.history.push('/');
    }
    if (error !== null) {
      console.log(error);
      clearErrors();
    }
  }, [error, isAuthenticated, props.history, clearErrors]);

  const [user, setUser] = useState({
    name: '',
    password: '',
  });
  const { name, password } = user;
  const onChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const onSubmit = e => {
    e.preventDefault();
    if (name === '' || password === '') {
      console.error('Please enter all fields');
    } else {
      loginRequest({ name, password });
    }
  };
  return (
    <Fragment>
      <Header />
      <ContentWrapper margin='128px 0 220px'>
        <Card width='30%'>
          <CardLabel>Login</CardLabel>
          <CardForm onSubmit={onSubmit}>
            <div className='form-group'>
              <label htmlFor='name'>User Name</label>
              <br />
              <FormInput
                type='text'
                name='name'
                placeholder='TonyStark'
                value={name}
                onChange={onChange}
                maxLength={30}
                pattern="[A-Za-z0-9-_\. ]+"
              />
            </div>
            <div className='form-group'>
              <label htmlFor='password'>Password</label>
              <br />
              <FormInput
                type='password'
                name='password'
                value={password}
                onChange={onChange}
                maxLength={30}
                pattern="[A-Za-z0-9-_\. ]+"
              />
              <img
                src={showPassIcon}
                alt='show-password'
                onClick={showHidePassword}
              />
            </div>
            <FormSubmit type='submit' value='Continue'/>
          </CardForm>
        </Card>
        <Card width="30%" flexDirection="row" height='75px' margin = '22px 0 0 0'>
          <span style={{color:"#2a2a2a"}}>I have no account,</span>
          <Link to='/register' style={redirectLabel}>
            Register now
          </Link>
        </Card>
      </ContentWrapper>
      <Footer />
    </Fragment>
  );
};
const mapStateToProps = state => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { loginRequest, clearErrors })(Login);
