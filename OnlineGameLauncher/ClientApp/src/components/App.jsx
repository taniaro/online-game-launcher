import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import Login from './auth/Login';
import Register from './auth/Register';
import Home from './pages/Home';
import PrivateRoute from '../components/routing/PrivateRoute';
import '../components/styles/App.scss';
import store from '../store/store';
import styled from 'styled-components';
import Game from './games/Game';
import UserProfile from './pages/UserProfile'
import Policy from './pages/Policy'

export const history = createBrowserHistory();

export const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
`;

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Wrapper>
          <Switch>
            <Route exact path='/login' component={Login} />
            <Route exact path='/register' component={Register} />
            <PrivateRoute exact path='/' component={Home} />
            <PrivateRoute exact path='/games/:id' component={Game} />
            <PrivateRoute exact path='/profile' component={UserProfile} />
            <Route exact path='/policy' component={Policy} />
          </Switch>
        </Wrapper>
      </Router>
    </Provider>
  );
};

export default App;
