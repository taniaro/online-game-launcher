import React, { Fragment } from 'react';
import Logo from '../../res/ea-sports.svg';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, GameItemWrapper, GameInfo, GameName } from '../StyledComponents';
import {deleteUserGameRequest} from '../../store/actions/user.game.actions'

const GameItem = props => {
  const { userGame,auth:{user},deleteUserGameRequest } = props;
  const { name, link, description } = userGame;
  const onClick = () => {
    window.open(link);
  };
  const deleteFromLibrary = () =>
  {
    deleteUserGameRequest(userGame.id);
  }
  return (
    <Fragment>
      <GameItemWrapper>
        <GameInfo>
            <img src={Logo} alt='game-logo' width='100' height='100' />
            <GameName>{name} </GameName>
        </GameInfo>
        <ul>
          <li>
            <Button backgroundColor='#3e3961;' onClick={onClick}>Start</Button>
            <Button  backgroundColor='#4fc2b7;' onClick={deleteFromLibrary}>Remove</Button>
          </li>
        </ul>
      </GameItemWrapper>
      <hr />
    </Fragment>
  );
};
const mapStateToProps = state => ({
  auth: state.auth,
  gamesState: state.games,
});
export default connect(mapStateToProps,{deleteUserGameRequest})(GameItem);
