import React, { Fragment, useEffect } from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import { getSingleGameRequest } from '../../store/actions/game.actions';
import { ContentWrapper } from '../StyledComponents';
import { connect } from 'react-redux';
const Game = props => {
  const {
    match,
    getSingleGameRequest,
    games: { currentGame },
  } = props;
  useEffect(() => {
    getSingleGameRequest(match.params.id);
  }, [getSingleGameRequest, match.params.id]);
  return (
    <Fragment>
      <Header />
      <ContentWrapper>
        {currentGame !== null ? (
          <Fragment>
            <ul style={{marginTop:'18px'}}>
              <li>Game Name: {currentGame.name}</li>
              <li>Game Description: {currentGame.description}</li>
            </ul>
          </Fragment>
        ) : null}
      </ContentWrapper>
      <Footer />
    </Fragment>
  );
};
const mapStateToProps = state => ({
  games: state.games,
});
export default connect(mapStateToProps, { getSingleGameRequest })(Game);
