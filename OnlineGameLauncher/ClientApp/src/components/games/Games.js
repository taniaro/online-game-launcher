import React, { useEffect, Fragment } from 'react';
import GameItem from './GameItem';
import { connect } from 'react-redux';
import { getGamesRequest } from '../../store/actions/game.actions';
import { getUserGamesRequest} from '../../store/actions/user.game.actions'
import { GamesWrapper } from '../StyledComponents';

const Games = ({ gamesState: { games }, auth:{user}, getGamesRequest,getUserGamesRequest }) => {
  useEffect(() => {
    getGamesRequest();
  }, [getGamesRequest]);
  useEffect(()=>{
    getUserGamesRequest(user.id);
  },[getUserGamesRequest,user.id,user])
  if (games !== null && games.length === 0) {
    return <h4>Please add games to be shown</h4>;
  }
  return (
    <GamesWrapper>
      {games !== null ? (
        <Fragment>
          {games.map(game => (
            <GameItem game={game} key={game.id} />
          ))}
        </Fragment>
      ) : null}
    </GamesWrapper>
  );
};
const mapStateToProps = state => ({
    gamesState: state.games,
    auth: state.auth
  });
export default connect(mapStateToProps,{getGamesRequest,getUserGamesRequest})(Games);
