import React, { Fragment } from 'react';
import Logo from '../../res/ea-sports.svg';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, GameItemWrapper, GameInfo, GameName } from '../StyledComponents';
import {addUserGameRequest} from '../../store/actions/user.game.actions'

const GameItem = props => {
  const { game,auth:{user},addUserGameRequest } = props;
  const { name, link, description } = game;
  const addToLibrary = () =>
  {
    const formData = {
      userId: user.id,
      gameId: game.id
    }
    addUserGameRequest(formData)
  }
  return (
    <Fragment>
      <GameItemWrapper>
        <GameInfo>
          <Link to={`games/${game.id}`}>
            <img src={Logo} alt='game-logo' width='100' height='100' />
          </Link>
          <GameName>{name} </GameName>
        </GameInfo>
            <Button  backgroundColor='#4fc2b7;' onClick={addToLibrary}>+</Button>
      </GameItemWrapper>
      <hr />
    </Fragment>
  );
};
const mapStateToProps = state => ({
  auth: state.auth,
});
export default connect(mapStateToProps,{addUserGameRequest})(GameItem);
