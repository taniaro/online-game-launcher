import styled from 'styled-components';

export const ContentWrapper = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: ${props => props.margin || '0'}
`

export const Card = styled.div`
    background-color: $primary-color;
    border-radius: 7px;
    box-shadow: 0px 2px 42px rgba(0, 0, 0, 0.111233);
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: ${props => props.flexDirection || 'column'};
    width: ${props => props.width || 'auto'};
    height: ${props => props.height || 'auto'};
    margin: ${props => props.margin || '0'};
    @media (max-width: 1024px) {
        width: 50%;
    };
    @media (max-width: 425px) {
        width: 75%;
    };
`
export const CardLabel = styled.div`
    color: #2b2b2b;
    font-size: 22px;
    text-align: center;
    padding: 25px 0px 20px;
`
export const CardForm = styled.form`
    margin-top: 20px;
    display: flex;
    flex-direction: column;
    width: 100%;
`
export const FormInput = styled.input`
    border: 1px solid #dedee0;
    border-radius: 5px;
    font-size: 16px;
    background-color: #f9faf8;
    width: 100%;
    height: 60px;
    padding: 0px 16px;
`
export const FormSubmit = styled.input`
    background-color: #349a89;
    height: 60px;
    margin: 20px 24px;
    font-size: 16px;
    cursor: pointer;
    color: #ffffff;
    border: none;
    border-radius: 4px;
    padding: 8px 25px;
    outline: none;
`
export const Button = styled.button`
    background-color: ${props => props.backgroundColor || '#349a89;'}
    height: 60px;
    margin: 20px 24px;
    font-size: 16px;
    cursor: pointer;
    color: #ffffff;
    border: none;
    border-radius: 4px;
    padding: 8px 25px;
    outline: none;
    text-transform: uppercase;
`

export const FooterComponent = styled.footer`
    width: 100%;
    background-color: rgba(192, 196, 206, 0.127321);
    text-align: center;
    padding: 20px 0 35px;
    font-size: 14px;
    color: #c0c4ce;
    word-spacing: 12px;
`
export const NavbarWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 80%;
    margin: 0 auto;
    padding: 8px 0px;
    @media (max-width: 768px) {
        flex-direction: column;
    }
`
export const GamesWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 60%;
    margin: 18px 0;
`
export const GameItemWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin: 18px;

`
export const GameInfo = styled.ul`
    display: flex;
    flex-direction: row;
`

export const GameName = styled.span`
    margin-top: 40px;
    margin-left: 30px;
    font-size: 25px;
`