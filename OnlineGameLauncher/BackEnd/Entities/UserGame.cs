﻿namespace OnlineGameLauncher.Entities
{
    public class UserGame
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
