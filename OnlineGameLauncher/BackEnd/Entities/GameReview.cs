﻿namespace OnlineGameLauncher.Entities
{
    public class GameReview
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }

        public string Picture { get; set; }
        public string Screenshot { get; set; }
    }
}
