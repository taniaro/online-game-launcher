﻿namespace OnlineGameLauncher.Entities
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string Screenshot { get; set; }
    }
}