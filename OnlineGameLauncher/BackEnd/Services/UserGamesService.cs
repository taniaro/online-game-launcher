﻿using AutoMapper;
using OnlineGameLauncher.Contexts;
using Contracts;
using OnlineGameLauncher.Entities;
using OnlineGameLauncher.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineGameLauncher.Services
{
    public interface IUserGamesService
    {
        UserGame AddUserGame(UserGameDto userGameDto);
        IEnumerable<GameDto> GetUserGames(int userId);
        void DeleteUserGame(int id);
    }

    public class UserGamesService : IUserGamesService
    {
        private readonly OGLDBContext _context;
        private IMapper _mapper;

        public UserGamesService(
            OGLDBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public UserGame AddUserGame(UserGameDto userGameDto)
        {
            if (_context.UserGames.Any(
                x => x.UserId == userGameDto.UserId
                && x.GameId == userGameDto.GameId))
                throw new AppException("Game already added");

            var userGame = _mapper.Map<UserGame>(userGameDto);

            _context.UserGames.Add(userGame);
            _context.SaveChanges();

            return userGame;
        }

        public IEnumerable<GameDto> GetUserGames(int userId)
        {
            var gameDtos = (from userGames in _context.UserGames
                            join games in _context.Games
                            on userGames.GameId equals games.Id
                            where userGames.UserId == userId
                            select new GameDto
                            {
                                Id = userGames.Id,
                                Name = games.Name,
                                Link = games.Link,
                                Description = games.Description,
                            }).ToList();

            return gameDtos;
        }

        public void DeleteUserGame(int id)
        {
            var userGame = _context.UserGames.Find(id);

            if(userGame != null)
            {
                _context.UserGames.Remove(userGame);
                _context.SaveChanges();
            }
        }
    }
}
