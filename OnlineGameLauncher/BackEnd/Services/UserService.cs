﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineGameLauncher.Contexts;
using OnlineGameLauncher.Entities;
using OnlineGameLauncher.Helpers;
using Contracts;
using Microsoft.AspNetCore.Http;
using System.IO;
using FluentValidation.Results;
using AutoMapper;

namespace OnlineGameLauncher.Services
{
    public interface IUserService
    {
        UserDto Authenticate(UserDto userDto);
        IEnumerable<UserDto> GetAll();
        UserDto GetById(int id);
        User Create(UserDto userDto);
        void Update(int id, UserDto userDto);
        void Delete(int Id);
        void AddPicture(int id, IFormFile photo);
        string GetPicture(int id);
    }

    public class UserService : IUserService
    {
        private OGLDBContext _context;
        private IMapper _mapper;

        public UserService(
            OGLDBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void AddPicture(int id, IFormFile photo)
        {
            var user = _context.Users.Find(id);
            string userPicturesFolder = @"C:\user-pictures";

            if(!Directory.Exists(userPicturesFolder))
                Directory.CreateDirectory(userPicturesFolder);

            string photoName = user.Name + DateTime.Now.ToString("yyyyMMdd") + photo.FileName;
            string fullPhotoPath = Path.Combine(userPicturesFolder, photoName);

            using (var fileStream = new FileStream(fullPhotoPath, FileMode.Create))
            {
                photo.CopyTo(fileStream);
            }

            user.Picture = fullPhotoPath;
            _context.SaveChanges();
        }

        public UserDto Authenticate(UserDto userDto)
        {
            if (userDto == null)
                throw new AppException("Error in your data");

            var user = _context.Users.SingleOrDefault(x => x.Name == userDto.Name);

            if(user == null)
                throw new AppException("User does not exist");

            if (!PasswordHelper.VerifyPasswordHash(userDto.Password, 
                user.PasswordHash, user.PasswordSalt))
                throw new AppException("Invalid password");

            var tokenString = TokenHelper.CreateTokenString(user, AppSettings.Secret);
            var userPicture = GetPicture(user.Id);

            var userDtoReturn = new UserDto
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Token = tokenString,
                Picture = userPicture
            };

            return userDtoReturn;
        }

        public User Create(UserDto userDto)
        {
            ValidateDto(userDto);

            var user = _mapper.Map<User>(userDto);

            CheckIfNameUnique(user.Name);
            CheckIfEmailUnique(user.Email);

            if (_context.Users.Any(x => x.Email == user.Email))
                throw new AppException("Email is already taken");

            PasswordHelper.CreateOrUpdatePassword(userDto.Password, ref user);
            user.Picture = @"C:\user-pictures\default.png";

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);

            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        public IEnumerable<UserDto> GetAll()
        {
            var users = _context.Users;
            var userDtos = _mapper.Map<IList<UserDto>>(users);

            return userDtos;
        }

        public UserDto GetById(int id)
        {
            var user =  _context.Users.Find(id);
            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public string GetPicture(int id)
        {
            var picturePath = _context.Users.Find(id).Picture;

            Byte[] bytes = File.ReadAllBytes(picturePath);
            String userPictureBase64 = Convert.ToBase64String(bytes);

            return userPictureBase64;
        }

        public void Update(int id, UserDto userDto)
        {
            ValidateDto(userDto);

            var userToEdit = _mapper.Map<User>(userDto);
            userToEdit.Id = id;

            var user = _context.Users.Find(id);

            if (userToEdit.Name != user.Name)
                CheckIfNameUnique(userToEdit.Name);

            if (userToEdit.Email != user.Email)
                CheckIfEmailUnique(userToEdit.Email);

            user.Name = userToEdit.Name;
            user.Email = userToEdit.Email;

            PasswordHelper.CreateOrUpdatePassword(userDto.Password, ref user);

            _context.Users.Update(user);
            _context.SaveChanges();
        }
        
        private void ValidateDto(UserDto userDto)
        {
            UserValidator validator = new UserValidator();
            ValidationResult results = validator.Validate(userDto);

            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    throw new AppException("Property " + failure.PropertyName +
                        " failed validation. Error was: " + failure.ErrorMessage);
                }
            }
        }

        private void CheckIfNameUnique(string name)
        {
            if (_context.Users.Any(x => x.Name == name))
                throw new AppException("Name is already taken");
        }

        private void CheckIfEmailUnique(string email)
        {
            if (_context.Users.Any(x => x.Email == email))
                throw new AppException("Email is already taken");
        }

    }
}
