﻿using AutoMapper;
using OnlineGameLauncher.Contexts;
using Contracts;
using OnlineGameLauncher.Entities;
using OnlineGameLauncher.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace OnlineGameLauncher.Services
{
    public interface IGameService
    {
        Game Create(GameDto gameDto);
        GameDto GetById(int id);
        IEnumerable<GameDto> GetAll();
        void Update(int id, GameDto gameDto);
        void Delete(int id);
    }

    public class GameService : IGameService
    {
        private OGLDBContext _context;
        private IMapper _mapper;

        public GameService(OGLDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Game Create(GameDto gameDto)
        {
            if (_context.Games.Any(x => x.Name == gameDto.Name))
                throw new AppException("Game with this name already exists");

            var game = _mapper.Map<Game>(gameDto);
            _context.Games.Add(game);
            _context.SaveChanges();

            return game;
        }

        public void Update(int id, GameDto gameDto)
        {
            var gameParams = _mapper.Map<Game>(gameDto);

            var game = _context.Games.Find(id);
            if (game == null)
                throw new AppException("Game not found");

            if (_context.Games.Any(x => x.Name == gameDto.Name))
                throw new AppException("Game with this name already exists");

            game.Name = gameParams.Name;
            game.Link = gameParams.Link;

            //TODO: update picture, description, screenshot
            //TODO: EntityState.Modified;

            _context.Games.Update(game);
            _context.SaveChanges();

        }

        public IEnumerable<GameDto> GetAll()
        {
            var games = _context.Games;
            var gameDtos = _mapper.Map<IList<GameDto>>(games);

            return gameDtos;
        }

        public GameDto GetById(int id)
        {
            var game = _context.Games.Find(id);
            var gameDto = _mapper.Map<GameDto>(game);

            return gameDto;
        }

        public void Delete(int id)
        {
            var game = _context.Games.Find(id);

            if (game != null)
            {
                _context.Games.Remove(game);
                _context.SaveChanges();
            }
        }
    }
}
