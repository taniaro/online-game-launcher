﻿using Microsoft.EntityFrameworkCore;
using OnlineGameLauncher.Entities;

namespace OnlineGameLauncher.Contexts
{
    public class OGLDBContext : DbContext
    {
        public OGLDBContext() : base (){ }

        public OGLDBContext(DbContextOptions<OGLDBContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<UserGame> UserGames { get; set; }
        public DbSet<GameReview> GameReviews { get; set; }
    }
}
