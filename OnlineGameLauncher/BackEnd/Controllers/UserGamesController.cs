﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineGameLauncher.Contexts;
using Contracts;
using OnlineGameLauncher.Entities;
using OnlineGameLauncher.Helpers;
using OnlineGameLauncher.Services;

namespace OnlineGameLauncher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserGamesController : ControllerBase
    {
        private readonly OGLDBContext _context;
        private readonly IUserGamesService _userGamesService;

        public UserGamesController
            (OGLDBContext context,
            IUserGamesService userGamesService)
        {
            _context = context;
            _userGamesService = userGamesService;
        }

        // POST: api/UserGames
        [HttpPost]
        public IActionResult AddUserGame([FromBody]UserGameDto userGameDto)
        {
            try
            {
                _userGamesService.AddUserGame(userGameDto);
                return Ok(_userGamesService.GetUserGames(userGameDto.UserId));
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // GET: api/UserGames/5
        [HttpGet("{userId}")]
        public IEnumerable<GameDto> GetUserGames(int userId)
        {
            return  _userGamesService.GetUserGames(userId);
        }

        // DELETE: api/UserGames/5
        [HttpDelete("{id}")]
        public IActionResult DeleteUserGame(int id)
        {
            try
            {
                _userGamesService.DeleteUserGame(id);
                return Ok(id);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
