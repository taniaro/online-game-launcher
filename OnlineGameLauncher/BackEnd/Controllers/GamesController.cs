﻿using Microsoft.AspNetCore.Mvc;
using Contracts;
using OnlineGameLauncher.Helpers;
using OnlineGameLauncher.Services;

namespace OnlineGameLauncher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private IGameService _gameService;

        public GamesController(IGameService gameService)
        {
            _gameService = gameService;
        }

        // POST: api/Games
        [HttpPost("creategame")]
        public IActionResult CreateGame([FromBody]GameDto gameDto)
        {
            try
            {
                _gameService.Create(gameDto);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // PUT: api/Games/5
        [HttpPut("{id}")]
        public IActionResult UpdateGame(int id, [FromBody]GameDto gameDto)
        {
            try
            {
                _gameService.Update(id, gameDto);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // GET: api/Games
        [HttpGet]
        public IActionResult GetGames()
        {
            return Ok(_gameService.GetAll());
        }

        // GET: api/Games/5
        [HttpGet("{id}")]
        public IActionResult GetGame(int id)
        {
            return Ok(_gameService.GetById(id));
        }

        // DELETE: api/Games/5
        [HttpDelete("{id}")]
        public IActionResult DeleteGame(int id)
        {
            _gameService.Delete(id);
            return Ok();
        }
    }
}
