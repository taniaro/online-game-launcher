﻿ALTER TABLE `ogldb`.`users`
DROP COLUMN `Picture`;

ALTER TABLE `ogldb`.`games`
DROP COLUMN `Picture`;

ALTER TABLE `ogldb`.`games`
DROP COLUMN `Screenshot`;

ALTER TABLE `ogldb`.`users` 
ADD COLUMN `Picture` VARCHAR(255) NULL DEFAULT 'C:\\user-pictures\\default.png' AFTER `PasswordSalt`;

ALTER TABLE `ogldb`.`games` 
ADD COLUMN `Picture` VARCHAR(255) NULL DEFAULT 'C:\\game-pictures\\default.png' AFTER `Description`;

ALTER TABLE `ogldb`.`games` 
ADD COLUMN `Screenshot` VARCHAR(255) NULL DEFAULT 'C:\\game-screens\\default.png' AFTER `Description`;

