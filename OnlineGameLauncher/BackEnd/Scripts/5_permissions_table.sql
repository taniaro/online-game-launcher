﻿CREATE TABLE `ogldb`.`userpermissions` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `PermissionGroup` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) VISIBLE,
  UNIQUE INDEX `PermissionGroup_UNIQUE` (`PermissionGroup` ASC) VISIBLE);


INSERT INTO `ogldb`.`userpermissions` (`Id`, `PermissionGroup`) VALUES ('1', 'Admin');
INSERT INTO `ogldb`.`userpermissions` (`Id`, `PermissionGroup`) VALUES ('2', 'User');

ALTER TABLE `ogldb`.`users` 
ADD COLUMN `Permission` INT NOT NULL DEFAULT 2 AFTER `Picture`;

/*якщо в базі вже є юзери, і їх Permission не 2:*/

SET SQL_SAFE_UPDATES = 0;
UPDATE users SET Permission = 2;

ALTER TABLE `ogldb`.`users` 
ADD CONSTRAINT `fk_permission`
  FOREIGN KEY (`Permission`)
  REFERENCES `ogldb`.`userpermissions` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



