﻿ALTER TABLE `ogldb`.`users` 
ADD COLUMN `Picture` VARCHAR(255) NULL DEFAULT 'C:/user-pictures/default.png' AFTER `PasswordSalt`;

CREATE TABLE `ogldb`.`games` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Link` VARCHAR(255) NOT NULL,
  `Description` VARCHAR(255) NULL,
  `Picture` VARCHAR(255) NULL DEFAULT 'C:/game-pictures/default.png',
  `Screenshot` VARCHAR(255) NULL DEFAULT 'C:/game-screens/default.jpg',
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) VISIBLE,
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) VISIBLE);

CREATE TABLE `ogldb`.`user_games` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `UserId` INT NOT NULL,
  `GameId` INT NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) VISIBLE,
  INDEX `fk_users_idx` (`UserId` ASC) VISIBLE,
  INDEX `fk_games_idx` (`GameId` ASC) VISIBLE,
  CONSTRAINT `fk_users`
    FOREIGN KEY (`UserId`)
    REFERENCES `ogldb`.`users` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_games`
    FOREIGN KEY (`GameId`)
    REFERENCES `ogldb`.`games` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

CREATE TABLE `ogldb`.`game_reviews` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `UserId` INT NOT NULL,
  `GameId` INT NOT NULL,
  `Rating` INT NULL,
  `Review` VARCHAR(255) NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) VISIBLE,
  INDEX `fk_users_idx` (`UserId` ASC) VISIBLE,
  INDEX `fk_games_idx` (`GameId` ASC) VISIBLE,
  CONSTRAINT `fk_users_rating`
    FOREIGN KEY (`UserId`)
    REFERENCES `ogldb`.`users` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_games_rating`
    FOREIGN KEY (`GameId`)
    REFERENCES `ogldb`.`games` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

