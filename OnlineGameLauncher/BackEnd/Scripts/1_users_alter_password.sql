﻿/*run it in your MySql workbench*/

ALTER TABLE ogldb.users 
CHANGE COLUMN PasswordHash PasswordHash VARBINARY(255) NOT NULL ,
CHANGE COLUMN PasswordSalt PasswordSalt VARBINARY(255) NOT NULL ;